package com.clientipcommunication;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ClientIpCommunicationApplication {

    public static void main(String[] args) {
        SpringApplication.run(ClientIpCommunicationApplication.class, args);
    }

}
