package com.clientipcommunication.controller;

import com.clientipcommunication.service.RequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
@Controller
public class HomeController {
    @Autowired
    private RequestService requestService;

    @RequestMapping("/")
    public ModelAndView index(HttpServletRequest request) {
        ModelAndView model = new ModelAndView("index");
        String clientIp = requestService.getClientIp(request);
        model.addObject("clientIp", clientIp);
        System.out.println(request.getRemoteAddr());
        return model;
    }
    @RequestMapping("/home")
    public String home(HttpServletRequest request)
    {
       String user= request.getRemoteUser();
        return user;
    }
}