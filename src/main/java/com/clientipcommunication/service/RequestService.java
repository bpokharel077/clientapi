package com.clientipcommunication.service;

import javax.servlet.http.HttpServletRequest;

public interface RequestService {
    String getClientIp(HttpServletRequest httpServletRequest);
}
